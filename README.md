### Install Chezmoi
```bash
docker run --rm -it -v .:/usr/bin cgr.dev/chainguard/wolfi-base:latest sh -c 'apk add chezmoi' 
```

### Apply it 
```bash
./chezmoi init --apply https://gitlab.com/kranurag7/dotfiles
```

### Inside container
```bash
apk add fish chezmoi; chezmoi init --apply https://gitlab.com/kranurag7/dotfiles; fish
```
