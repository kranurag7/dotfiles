function mkcd
    mkdir $argv && cd $argv
end

abbr -ag rem --set-cursor 'rm -f $(command -v %)'
alias .. 'cd ..'
alias ... 'cd ../..'
alias .... 'cd ../../..'
alias ..... 'cd ../../../..'
