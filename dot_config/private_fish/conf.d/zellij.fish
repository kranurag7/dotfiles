if command -v zellij &>/dev/null
    zellij_tab_name_update
    abbr -ag zl zellij
    abbr -ag zlc 'zellij action close-tab'
    abbr -ag zlp 'zellij action close-pane'
    abbr -ag ze 'zellij edit'
end
