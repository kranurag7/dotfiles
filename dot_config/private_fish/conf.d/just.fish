if command -v just &>/dev/null
    just --completions fish | source
    abbr -ag j just
    abbr -ag je 'just -e'
    abbr -ag .j 'just -g'
    abbr -ag .je 'just -g --edit'
end
