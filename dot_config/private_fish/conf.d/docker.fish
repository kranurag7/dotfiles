if command -v docker &>/dev/null
    abbr -ag de 'docker exec -it'
    abbr -ag drm 'docker run --rm'
    abbr -ag dps 'docker ps'
    abbr -ag dsh 'docker run --rm -it --entrypoint=sh'
    abbr -ag dbash 'docker run --rm -it --entrypoint=bash'
    abbr -ag did 'docker run --rm --entrypoint=id'
end
