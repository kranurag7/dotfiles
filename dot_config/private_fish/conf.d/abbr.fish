abbr -ag rmssh 'rm ~/.ssh/known_hosts ~/.ssh/known_hosts.old'
abbr -ag rem --set-cursor 'rm -f $(command -v %)'
