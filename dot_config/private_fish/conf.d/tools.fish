# let's have all the tools that ship with good defaults here.

# ripgrep
if command -v rg &>/dev/null
    abbr -ag rgg 'rg -g !vendor'
    abbr -ag rgh 'rg --hidden'
    abbr -ag rgf 'rg -F'
    set -x RIPGREP_CONFIG_PATH $HOME/.config/rg/ripgreprc
end

# atuin
if command -v atuin &>/dev/null
    atuin init fish | source
end

# zoxide
if command -v zoxide &>/dev/null
    zoxide init fish | source
end

# eza
if command -v eza &>/dev/null
    abbr -ag ll 'eza -halo'
    alias ls eza
end

# mktemp
if command -v mktemp &>/dev/null
    abbr -ag gtmp 'cd $(mktemp --directory --tmpdir hacky_XXXXXX)'
end
