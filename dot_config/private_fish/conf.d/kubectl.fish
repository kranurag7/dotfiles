### kubectl
if command -v kubectl &>/dev/null

    function pod_uuidgen
        echo kubectl run --rm -it --image=cgr.dev/chainguard/wolfi-base (uuidgen | cut -c26-) --restart=Never sh
    end

    kubectl completion fish | source
    abbr -ag k kubectl
    abbr -ag kn 'kubectl -n'
    abbr -ag kgpse 'kubectl get pods,svc,ep'
    abbr -ag kgcs 'kubectl get cm,secret'
    abbr -ag kg 'kubectl get'
    abbr -ag kdel 'kubectl delete'
    abbr -ag kd 'kubectl describe'
    abbr -ag kgp 'kubectl get pods'
    abbr -ag kgpk 'kubectl get pods -n kube-system'
    abbr -ag kgpa 'kubectl get pods -A'
    abbr -ag kgpw 'kubectl get pods -owide'
    ### nodes
    abbr -ag kgno 'kubectl get nodes'
    abbr -ag kgnow 'kubectl get nodes -owide'
    abbr -ag kgns 'kubectl get ns'
    abbr -ag kga 'kubectl get all'
    abbr -ag kgaa 'kubectl get all -A'
    abbr -ag kaf 'kubectl apply -f'
    abbr -ag kdelf 'kubectl delete -f'
    abbr -ag kgs 'kubectl get svc'
    abbr -ag kgsa 'kubectl get svc -A'
    abbr -ag kref 'kubectl replace --force -f '
    abbr -ag kgd 'kubectl get deployments'
    abbr -ag kgda 'kubectl get deployments -A'
    alias rmkube 'rm -f ~/.kube/config'
    alias rmkubetmp 'rm -f /tmp/kubectl-edit-*'
    ### secrets 
    abbr -ag -g kgsec 'kubectl get secrets'
    abbr -ag -g kgseca 'kubectl get secrets -A'
    ### daemonsets 
    abbr -ag kgds 'kubectl get daemonsets'
    abbr -ag kgdsa 'kubectl get daemonsets -A'
    ### endpoints 
    abbr -ag kgep 'kubectl get endpoints'
    ### ingress 
    abbr -ag kgi 'kubectl get ing'
    abbr -ag kgip 'kubectl get ing -o=custom-columns='NAME:.metadata.name,SVCs:..service.name,PATH:..paths[].path,PORT:..service.port' -A'
    ### events
    abbr -ag kev 'kubectl events'
    abbr -ag keva 'kubectl events -A'
    abbr -ag kevw 'kubectl events --type=Warning'
    abbr -ag kgevt 'kubectl get ev'
    ### configmaps
    abbr -ag kgcm 'kubectl get cm'
    ### debug
    abbr -ag kshell --function pod_uuidgen
    ### capi related 
    abbr -ag kgcl 'kubectl get clusters'
    abbr -ag kgcla 'kubectl get clusters -A'
    abbr -ag kgcc 'kubectl get clusterclass'
    abbr -ag kgcca 'kubectl get clusterclass -A'
    abbr -ag kgma 'kubectl get machines'
    abbr -ag kgmaa 'kubectl get machines -A'
    abbr -ag kgmd 'kubectl get machinedeployments'
    abbr -ag kgkcp 'kubectl get kubeadmcontrolplane'
    abbr -ag kgkcpa 'kubectl get kubeadmcontrolplane -A'
    abbr -ag kgmda 'kubectl get machinedeployments -A'
end
